from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.form import Form, MyForm


# Create your views here.


def todo_list_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_list": todo,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_details = get_object_or_404(TodoList, id=id)
    context = {
        "todo_details": todo_details,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = Form(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = Form()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    id = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = Form(request.POST, instance=id)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id.id)
    else:
        form = Form(instance=id)

    context = {
        "form": form,
        "id": id,
    }
    return render(request, "todos/edit.html", context)


def todo_list_detele(request, id):
    # if request.method == "POST":
    #     list = TodoList.objects.get(id=id)
    #     list.delete()
    #     return redirect("todo_list_list")

    delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":

        form = MyForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = MyForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = MyForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = MyForm(instance=item)
    context = {
        "item": item,
        "form": form,
    }
    return render(request, "todos/edit.html", context)
